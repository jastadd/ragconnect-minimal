# Simple Tree Demo

This demonstration is an interactive demo.
It requires you to publish messages by hand. This can be achieved by using e.g. [mqtt-spy](https://kamilfb.github.io/mqtt-spy/) or the command line tools from mosquitto (`mosquitto_pub`).

The demo creates a tiny AST of the form:

```
Root
|- A
   |- Input:String
|- B
   |- (computed) OutputOnB:String
```

The "Input" token is connected to MQTT topic `input`, i.e., whenever a new message is received on that topic, the content of the message is written into that token.

Everytime the Input token is updated, the computed property "OutputOnB" is recalculated.
Then its content is to be sent to topic `output`, but only after applying a (simple) transformation appending `"postfix"`.
