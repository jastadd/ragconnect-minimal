# RagConnect Minimal Example

This repository is used to show three examples using [RagConnect](https://jastadd.pages.st.inf.tu-dresden.de/ragconnect/).
All examples can be run without prior knowledge of RagConnect or Attribute Grammars, but you are encouraged to change inputs (grammar, attributes, connect specifications), recompile and observe the resulting changes.

All three demos require a running MQTT broker on your machine (the application will abort if no connection is possible).
We recommend [mosquitto](https://mosquitto.org/) as it is easy to set up.

To visualize the complete grammar, the gradle task `grammar2uml` can be used, which produces the file `uml.png` containing the visualization.
