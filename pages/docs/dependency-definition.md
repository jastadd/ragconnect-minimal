# Dependency Definition Demo

This demo requires some setup in order to showcase its use case - the dependency definitions.
As all demos are based on the same code base, those changes have to be done manually.

First disable incremental evaluation by commenting out parameters for incremental evaluation (lines 76 and 143 in `build.gradle`):

```
            '--incremental=param,debug',
            "--incremental=param,debug",
```

Then comment in the dependency definitions in the connect specification (lines 15-17 in `src/main/jastadd/MinimalModel.connect`) to look like:

```
A.OutputOnA canDependOn A.Input as dependencyA ;
B.OutputOnB canDependOn A.Input as dependencyB ;
C.OutputOnC canDependOn A.Input as dependencyC ;
```

And finally comment in the usage of dependency definitions in the main class (lines 133-138 in `src/main/java/de/tudresden/inf/st/mrc/MinimalMain.java`) to look like:

```
// a.OutputOnA -> a.Input
normalA.addDependencyA(normalA);
// b1.OutputOnB -> a.Input
b1.addDependencyB(normalA);
// b2.OutputOnB -> a.Input
b2.addDependencyB(normalA);
```

The demonstration will show, how dependency definitions can be used in case incremental evaluation is not used.
