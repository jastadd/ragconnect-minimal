# Automatic Incremental Demo

This demonstration shows receiving and send of a complete subtree (of a type named `Alfa`).
The subtree has various children and relations within the subtree.

There are two separate trees ("sender" and "receiver").
The sender has a token "Input" which the subtree that is sent depends on.
That means, whenever this token is changed, a new subtree is calculated and sent out.

When running, three visualizations will be generated, one for the initial state (`00-initial-*.png`), one after setting the Input to "1" (`01-after-1-*.png`), and a last setting Input to "2" (`02-after-2-*.png`).
They show sender and receiver, respectively.
Furthermore, the console output also show sender and receiver.
